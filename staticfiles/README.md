# 3D parallax effect

## Description
This is a Django website template with a 3D parallax effect. It provides a visually appealing scrolling effect that creates an illusion of depth and immersion on web pages.

## Installation
To get started with this Django website template, follow these steps:
1. Clone this repository `git clone https://gitlab.com/_cesar_/3d-parallax-effect.git`
2. Create a new vitual environment and activate it: `virtualenv venv` `source venv/bin/activate`
3. Update PIP: `pip install --upgrade pip`
4. Install required dependencies: pip install -r requirements.txt

## Usage
### Start the Django development server
`python manage.py runserver`
- Open your web browser and visit 'http://localhost:8000' to view the website.

## License
GNU GPL v3.0
