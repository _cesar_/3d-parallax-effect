from django.shortcuts import render
import datetime

# Create your views here.
thisYear = datetime.datetime.now()

def index(request):
    context = {'year': thisYear}
    return render(request, 'landing/index.html', context)
